VERSION:=$(shell cat VERSION)

build:
	docker build \
		-t southclaws/minecraft \
		.

eula.txt:
	echo "eula=true" > eula.txt

test-run:
	DATA_DIR=./data VERSION=$(VERSION) docker-compose up -d

test-run-attached:
	DATA_DIR=./data VERSION=$(VERSION) docker-compose exec minecraft bash

prod-run:
	DATA_DIR=/data VERSION=$(VERSION) docker-compose up -d

prod-run-attached:
	DATA_DIR=/data VERSION=$(VERSION) docker-compose up
